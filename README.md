# Chicago Project #

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fopiticalvin%40bitbucket.org%2Fopiticalvin%2Fchicago-std-spatial-analysis.git/fad8300e650b24bafafa848c3d1076dfe6054b9e)

This is a sample spatial analysis project - a mini-project - for a course I undertook during my 
Master of Science - GIS and Remote Sensing at [The University of Witwatersrand](https://wits.ac.za)

### Analysis of Socio-Economic Factors Influencing the Spatial Distribution of STDs: A case study of City of Chicago ###

This repository breaks down the steps undergone leading towards the spatial analysis of the social and economic factors that contribute towards the prevalence and spread of Sexually Transmitted Diseases (STDs) in the City of Chicago, USA.

This breakdown is represented in the form of the following notebooks:
* [Preliminary Data Exploration and Management](01-Preliminary-Data-Exploration.ipynb)
* [Exploratory Spatial Data Analysis](02-Exploratory-Spatial-Data-Analysis.ipynb)
* [Regression Analysis](03-Spatial-n-Regression-Analysis.ipynb)

## Downloading this Repository and its materials

**Note**: *I am still in the process of updating and finalizing these notebooks. This being said, do feel free to update your local copy, through a `git pull` command if you are using git.*

If you have git installed, you can get these notebooks by cloning this repo:

    git clone https://opiticalvin@bitbucket.org/opiticalvin/chicago-std-spatial-analysis.git

Otherwise, you can download the repository as a .zip file by heading over
to the Bitbucket repository (https://opiticalvin@bitbucket.org/opiticalvin/chicago-std-spatial-analysis) in
your browser and click the "Download" button in the upper right:

![](img/download_repo.png)


### To Get in touch ###

Please feel free to hit me up on my email - (opiticalvin@gmail.com)